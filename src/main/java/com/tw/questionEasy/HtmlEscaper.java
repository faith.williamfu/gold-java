package com.tw.questionEasy;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HtmlEscaper {
    // TODO:
    //   You can add additional members or blocks of code here if you want.
    // <-start-
    // --end-->

    /**
     * This function will try escaping characters according to the rules defined in HTML 4.01
     * The rules are as follows:
     *
     * (1) Every `"` character will be escaped to `&quot;`
     * (2) Every `'` character will be escaped to `&#39;`
     * (3) Every `&` character will be escaped to `&amp;`
     * (4) Every `<` character will be escaped to `&lt;`
     * (5) Every `>` character will be escaped to `&gt;`
     *
     * @param text The text to escape.
     * @return The escaped string.
     */
    public static String escape(String text) {
        // TODO:
        //   Please implement the method
        // <-start-
        // I used replaceAll method at first but it cannot change all characters at the same time.
        // The following is the solution when I searched how to replace all characters at the same time.
        // Though all test passed, I cannot fully understand this part of code.
        if (text == null) {
            throw new IllegalArgumentException();
        }
        Pattern pattern = Pattern.compile("[\"'&<>]");
        Matcher matcher = pattern.matcher(text);
        StringBuffer escape = new StringBuffer();
        while (matcher.find()) {
            switch (matcher.group()) {
                case "\"":
                    matcher.appendReplacement(escape, "&quot;");
                    break;
                case "'":
                    matcher.appendReplacement(escape, "&#39;");
                    break;
                case "&":
                    matcher.appendReplacement(escape, "&amp;");
                    break;
                case "<":
                    matcher.appendReplacement(escape, "&lt;");
                    break;
                case ">":
                    matcher.appendReplacement(escape, "&gt;");
                    break;
            }
        }
        matcher.appendTail(escape);
        return escape.toString();
        // --end-->
    }
}
