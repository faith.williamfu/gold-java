package com.tw.questionMedium;

import java.util.*;
import java.util.stream.Collectors;

public class Library {
    private final List<Book> books = new ArrayList<>();

    public void addAll(Book ...books) {
        this.books.addAll(Arrays.asList(books));
    }

    /**
     * Find all the books which contains one of the specified tags.
     *
     * @param tags The tag list.
     * @return The books which contains one of the specified tags. The returned books
     *   should be ordered by their ISBN number.
     */
    public List<Book> findBooksByTag(String ...tags) {
        // TODO:
        //   Please implement the method
        // <-start-
        if (tags == null) {
            return null;
        }
        List<Book> targetBooks = new ArrayList<>();
        for (String tag : tags) {
            for (Book book : books) {
                if (book.getTags().contains(tag)) {
                    targetBooks.add(book);
                }
            }
        }
        List<Book> distinctBooks = targetBooks.stream().distinct().collect(Collectors.toList());
        // Sorting the list has not been completed.
        distinctBooks.sort(new Comparator<Book>() {
            @Override
            public int compare(Book s1, Book s2) {
                return s1.getIsbn().compareTo(s2.getIsbn());
            }
        });
        return distinctBooks;
        // --end-->
    }

    // TODO:
    //   You can add additional methods here if you want.
    // <-start-

    // --end-->
}
